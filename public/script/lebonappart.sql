-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 29 Octobre 2015 à 18:54
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lebonappart`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE IF NOT EXISTS `annonce` (
  `id_annonce` int(11) NOT NULL AUTO_INCREMENT,
  `idTypeBien` int(11) NOT NULL,
  `nomAnnonce` varchar(255) NOT NULL,
  `datePublication` date NOT NULL,
  `dateMiseAJour` date NOT NULL,
  `idVendeur` int(11) NOT NULL,
  `numeroRue` int(11) NOT NULL,
  `nomRue` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `codePostal` int(5) NOT NULL,
  `idTypeAnnonce` int(11) NOT NULL,
  `superficie` int(11) NOT NULL,
  `prixVente` double NOT NULL,
  `prixLoyerMensuel` double NOT NULL,
  `descriptif` text NOT NULL,
  `nbPieces` int(2) NOT NULL,
  `garage` tinyint(1) NOT NULL,
  `piscine` tinyint(1) NOT NULL,
  `jardin` tinyint(1) NOT NULL,
  `motDePasseAcces` text NOT NULL,
  PRIMARY KEY (`id_annonce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `annonce`
--

INSERT INTO `annonce` (`id_annonce`, `idTypeBien`, `nomAnnonce`, `datePublication`, `dateMiseAJour`, `idVendeur`, `numeroRue`, `nomRue`, `ville`, `codePostal`, `idTypeAnnonce`, `superficie`, `prixVente`, `prixLoyerMensuel`, `descriptif`, `nbPieces`, `garage`, `piscine`, `jardin`, `motDePasseAcces`) VALUES
(1, 2, 'Location Maison 7 pièces 130,11m² Paris 14ème', '2015-10-29', '2015-10-29', 10, 4, 'Place des Cinq Martyrs du Lycée Buffon', 'Paris-15E-Arrondissement', 75015, 1, 130, -1, 4150, 'Maison sur 3 étages donnant sur une cour privative arborée. Elle comprend: Au rez-de-chaussée: un séjour, une chambre, un dressing et une véranda dans laquelle se trouve une cuisine et une salle d''eau avec WC. Au 1er étage: 3 chambres. Au 2ème étage: sous une grande verrière, une suite parentale avec salle de bains, salon et mezzanine. L''ensemble est d''une surface habitable de 130.11m² et d''une surface au sol de 162.22m². Nombreux rangements. Disponible immédiatement.', 7, 1, 0, 1, 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
(2, 2, 'Location Maison 4 pièces 85m² Nancy ', '2015-10-29', '2015-10-29', 11, 15, 'Place de la Commanderie', 'Nancy', 54000, 1, 85, -1, 820, 'Coup de cœur assuré pour cette charmante maison entièrement rénovée offrant entrée, cuisine équipée ouverte sur salon-séjour, 3 chambres, salle d''eau avec WC.. Chauffage individuel électrique.. Une place de parking..Coup de cœur assuré pour cette charmante maison entièrement rénovée offrant entrée, cuisine équipée ouverte sur salon-séjour, 3 chambres, salle d''eau avec WC.. Chauffage individuel électrique.. Une place de parking..', 4, 1, 0, 0, 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
(3, 2, 'Location Maison / Villa 101m² Nancy ', '2015-10-29', '2015-10-29', 12, 32, 'Place de la République', 'Nancy', 54000, 1, 101, -1, 850, 'MAISON DE VILLE RÉCENTE DANS QUARTIER CALME PROCHE TOUTES COMMODITÉES rez-de-chaussée: hall d''entrée avec placard, WC, dégagement, cuisine équipée ouverte sur salon/séjour, WC ÉTAGE: palier, 4 chambres dont 2 avec placard, salle de bains, WC jardin, abri de jardin, garage et terrasse Toutes nos offres.', 6, 1, 0, 1, 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
(4, 2, 'Vente de prestige Maison / Villa 156m² Le Cap d Agde ', '2015-10-29', '2015-10-29', 13, 2, 'Rue de la Garnison', 'Agde', 34300, 2, 156, 299000, -1, 'L''Île des Marinas, un lieu privé et calme donnant l''accès direct à la plage et aux commodités. Cette villa vous offre une terrasse de 25 m² avec une vue exceptionnelle sur votre ponton privé, 2 grandes chambres, séjour, cuisine, salle de bains, WC. Parking sécurisé. Copropriété de 162 lots Charges annuelles: 793 euros.', 4, 1, 1, 1, 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
(5, 1, 'Vente appartement 4 pièces à Brest', '2015-10-29', '2015-10-29', 14, 24, 'Rue Coat ar Gueven', 'Brest', 29200, 2, 63, 88395, -1, 'Vendu avec un garage, à deux pas du Pont de Recouvrance et du Tramway. Dans une copropriété en dalle béton, appartement T4 rénové. A proximité des commerces et des transports en communes, salon-séjour de 28 m2 sur parquet donnant sur la cuisine aménagée. 2 Chambres également sur parquet. salle d''eau récente. Placards. Cave. Un garage fermé à deux pas. Fenêtres PVC DV. Chauffage gaz individuel. ', 4, 1, 0, 0, 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'),
(6, 1, 'Location appartement 2 pièce Lyon', '2015-10-29', '2015-10-29', 15, 34, 'Rue de la République', 'Lyon-2E-Arrondissement', 69002, 1, 45, -1, 950, 'Appartement traversant entièrement meublé au 3ème étage sans ascenseur. Il comprenant une cuisine ouverte sur un séjour calme et lumineux, une chambre donnant sur cour intérieure, une salle de bains double vasque.\r\n\r\nSituation idéale à proximité des commerces et des transports en communs.', 2, 0, 0, 0, 'a030981ebfbc5985d2d4658e83628d8be3bcafce'),
(7, 2, 'Vente Maison 5 pièce à Montpellier', '2015-10-29', '2015-10-29', 16, 14, 'Avenue de la Liberté', 'Montpellier', 34070, 2, 146, 650000, -1, 'Montpellier CENTRE-VILLE - A proximité de la gare et du Tramway, dans un secteur pavillonnaire, venez découvrir cette belle maison ancienne, au calme, avec un jardin arboré exposé plein Sud. Au RDC, elle se compose d’un vaste séjour avec cuisine entièrement équipée, le tout ouvert sur l’extérieur, d’un espace bureau et d’un WC. A l’étage, disposez de 3 chambres dont une avec dressing et d’une salle de bains avec douche, baignoire et double vasque. Vous serez séduits par sa rénovation alliant modernité et charme de l’ancien. Une cave de 75m² vient compléter ce bien de qualité !', 5, 1, 0, 1, 'a030981ebfbc5985d2d4658e83628d8be3bcafce'),
(8, 2, 'Vente Maison 6 pièces Montpellier', '2015-10-29', '2015-10-29', 17, 2, 'Avenue Marie de Montpellier', 'Montpellier', 34000, 2, 135, 450000, -1, 'Montpellier Quartier NOUVELLE MAIRIE - Au calme, dans un quartier très prisé, non loin du centre-ville et du Tram, magnifique maison 6 pièces d''environ 135m² avec jardin clos et arboré. Très bien entretenue avec des prestations de qualité, elle comprend un grand séjour, 3 chambres, un bureau, une salle de bain, une salle d''eau et de nombreux rangements intérieurs et extérieurs. Disposez également d''un studio indépendant supplémentaire avec kitchenette et mezzanine. Toiture révisée, chaudière neuve, isolation, peintures et façade extérieure refaites !', 6, 1, 1, 1, 'a030981ebfbc5985d2d4658e83628d8be3bcafce'),
(9, 2, 'Vente maison 10 pièces Metz', '2015-10-29', '2015-10-29', 18, 14, 'Place de France', 'Metz', 57000, 2, 165, 424000, -1, 'Grande maison F9 d''environ 165m² actuellement aménagée en bureaux, sur 2 niveaux avec 2 entrées indépendantes.\r\nGarage de 24m² aménagé en bureau, entrée, séjour, cuisine équipée semi-ouverte, 6 chambres/bureaux...\r\nLe tout sur un grand terrain de 994m² qui jouxte un axe passant.\r\n\r\nIdéal Investisseurs pour faire plusieurs logements ou faire un petit immeuble ou encore pour activités commerciales !', 10, 1, 0, 1, 'a030981ebfbc5985d2d4658e83628d8be3bcafce'),
(10, 1, 'Vente Maison 5 pièces Lille', '2015-10-29', '2015-10-29', 19, 14, 'Avenue Foch', 'Lille', 59000, 2, 100, 138000, -1, 'Située dans le quartier Mont de Terre à proximité d''une station de métro, maison des années 30 composée d''un hall d''entrée, salon / séjour, cuisine, salle de bain, Wc séparés, véranda, 4 chambres, agréable jardin. Prévoir un rafraîchissement.', 5, 1, 0, 1, 'a030981ebfbc5985d2d4658e83628d8be3bcafce');

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `ordre` int(11) NOT NULL,
  `intitule` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_menu`),
  UNIQUE KEY `ordre` (`ordre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `menu`
--

INSERT INTO `menu` (`id_menu`, `ordre`, `intitule`, `url`) VALUES
(1, 1, 'Accueil', '/'),
(2, 2, 'Déposer une annonce', '/ajout'),
(3, 3, 'Recherche', '/recherche'),
(4, 4, 'Notre agence', '/nous');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id_photo` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `id_annonce` int(11) NOT NULL,
  PRIMARY KEY (`id_photo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `url`, `id_annonce`) VALUES
(1, '../public/uploads/annonce_1_image_1_Photo1.jpg', 1),
(2, '../public/uploads/annonce_1_image_2_Photo2.jpg', 1),
(3, '../public/uploads/annonce_1_image_3_Photo3.jpg', 1),
(4, '../public/uploads/annonce_2_image_1_Photo1.jpg', 2),
(5, '../public/uploads/annonce_2_image_2_Photo2.jpg', 2),
(6, '../public/uploads/annonce_2_image_3_Photo3.jpg', 2),
(7, '../public/uploads/annonce_3_image_1_Photo1.jpg', 3),
(8, '../public/uploads/annonce_3_image_2_Photo2.jpg', 3),
(9, '../public/uploads/annonce_3_image_3_Photo3.jpg', 3),
(10, '../public/uploads/annonce_4_image_1_Photo1.jpg', 4),
(11, '../public/uploads/annonce_4_image_2_Photo2.jpg', 4),
(12, '../public/uploads/annonce_4_image_3_Photo3.jpg', 4),
(13, '../public/uploads/annonce_5_image_1_Photo1.jpg', 5),
(14, '../public/uploads/annonce_5_image_2_Photo2.jpg', 5),
(15, '../public/uploads/annonce_5_image_3_Photo3.jpg', 5),
(16, '../public/uploads/annonce_6_image_1_appartement-salon-1267379245.jpg', 6),
(17, '../public/uploads/annonce_6_image_2_canapes-en-tissus-et-tv-1267379338.jpg', 6),
(18, '../public/uploads/annonce_6_image_3_table-en-verre-sam-1267379544.jpg', 6),
(19, '../public/uploads/annonce_7_image_1_038D02BC03698622-photo-appartement-romantique-moderne-paris-104.jpg', 7),
(20, '../public/uploads/annonce_7_image_2_038D02BC03756872-photo-maison-normande-avec-jardin-140.jpg', 7),
(21, '../public/uploads/annonce_7_image_3_Appartement_de_standing_34322.jpg', 7),
(22, '../public/uploads/annonce_8_image_1_Maison_vente_Uccle_18.jpg', 8),
(23, '../public/uploads/annonce_8_image_2_Maison_vente_Uccle_B_05.jpg', 8),
(24, '../public/uploads/annonce_8_image_3_Maison_vente_Uccle_B_07.jpg', 8),
(25, '../public/uploads/annonce_9_image_1_Maison_vente_Uccle_B_11.jpg', 9),
(26, '../public/uploads/annonce_9_image_2_Maison_vente_Uccle_B_12.jpg', 9),
(27, '../public/uploads/annonce_9_image_3_Maison_vente_Uccle_B_02.jpg', 9),
(28, '../public/uploads/annonce_10_image_1_p1050216.jpg', 10),
(29, '../public/uploads/annonce_10_image_2_photos_maison_003.jpg', 10),
(30, '../public/uploads/annonce_10_image_3_photos_maison_011.jpg', 10);

-- --------------------------------------------------------

--
-- Structure de la table `typeannonce`
--

CREATE TABLE IF NOT EXISTS `typeannonce` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `typeannonce`
--

INSERT INTO `typeannonce` (`id_type`, `intitule`) VALUES
(1, 'Location'),
(2, 'Vente');

-- --------------------------------------------------------

--
-- Structure de la table `typebien`
--

CREATE TABLE IF NOT EXISTS `typebien` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `typebien`
--

INSERT INTO `typebien` (`id_type`, `intitule`) VALUES
(1, 'Appartement'),
(2, 'Maison');

-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

CREATE TABLE IF NOT EXISTS `vendeur` (
  `id_vendeur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  PRIMARY KEY (`id_vendeur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `vendeur`
--

INSERT INTO `vendeur` (`id_vendeur`, `nom`, `prenom`, `email`, `telephone`) VALUES
(10, 'Mangin', 'Romaric', 'romaric.mangin0947@gmail.com', '0672667584'),
(11, 'Cahp', 'Lucas', 'lucas.cahp@gmail.com', '0745389425'),
(12, 'Guilett', 'Gertrude', 'gertrude.guilett@yahoo.fr', '0645941423'),
(13, 'Barbier', 'Anthony', 'anthony.barbier@gmail.com', '0745255698'),
(14, 'Vuillaume', 'Corentin', 'corentin.vuillaume@laposte.net', '0612324789'),
(15, 'Pierre', 'Richard', 'richard_pierre@lebonappart.fr', '0006060606'),
(16, 'Bernard', 'Pascal', 'pascal_bernard@lebonappart.fr', '0007070707'),
(17, 'Grandiose', 'Pascal', 'pascal_grandiose@lebonappart.fr', '0008080808'),
(18, 'Bernard', 'Pascal', 'pascal_bernard@lebonappart.fr', '0007070707'),
(19, 'Grandiose', 'Pascal', 'pascal_grandiose@lebonappart.fr', '0008080808');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
