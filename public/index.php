<?php
/**
 * Created by PhpStorm.
 * User: Romaric Mangin
 * Date: 10/21/2015
 * Time: 10:52 AM
 */

require_once('../vendor/autoload.php');

session_start();

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../app/views'
));

require '../app/configuration/routes.php';

$app->run();

?>