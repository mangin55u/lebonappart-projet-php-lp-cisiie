/**
 * Created by Romaric on 25/10/2015.
 */
function validateForm() {
    // Check vendeur
    var nom = document.getElementById("nom").value;
    var prenom = document.getElementById("prenom").value;
    var mail = document.getElementById("email").value;
    var numero = document.getElementById("number").value;

    var nbErrors = 0;

    if (!nom.match(/^[a-zA-Z\-]+$/)) {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form").innerHTML = "Erreur : Le format du champ nom est invalide !";
        nbErrors++;
        return false;
    }

    if (!prenom.match(/^[a-zA-Z\-]+$/)) {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form").innerHTML = "Erreur : Le format du champ pr&eacute;nom est invalide !";
        nbErrors++;
        return false;
    }

    if (!numero.match(/^\d{10}$/)) {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form")
            .innerHTML = "Erreur : Le format du champ t&eacute;l&eacute;phone est invalide !";
        nbErrors++;
        return false;
    }

    // Check annonce
    var nbPieces = document.getElementById("pieces").value;
    var selectAnnonce = document.getElementById("tannonce").value;
    var superficie = document.getElementById("superficie").value;

    if (selectAnnonce == 0) {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form")
            .innerHTML = "Erreur : Le type de l'annonce n'est pas s&eacute;lection&eacute; !";
        nbErrors++;
        return false;
    }
    if (selectAnnonce == 2) {
        var select = document.getElementById("pvente").value;
        if (parseInt(select) <= 0 || select == "") {
            document.getElementById('error-form').style.display = 'inherit';
            document.getElementById("error-form").innerHTML = "Erreur : Le format du champ prix de vente est invalide !";
            nbErrors++;
            return false;
        }
    }
    if (selectAnnonce == 1) {
        var select = document.getElementById("ploc").value;
        if (parseInt(select) <= 0 || select == "") {
            document.getElementById('error-form').style.display = 'inherit';
            document.getElementById("error-form").innerHTML = "Erreur : Le format du champ prix de location est invalide !";
            nbErrors++;
            return false;
        }
    }

    if (parseInt(nbPieces) <= 0 || nbPieces == "") {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form")
            .innerHTML = "Erreur : Le format du champ nombre de pi&egrave;ces est invalide !";
        nbErrors++;
        return false;
    }

    if (parseInt(superficie) <= 0 || superficie == "") {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form").innerHTML = "Erreur : Le format du champ superficie est invalide !";
        nbErrors++;
        return false;
    }

    // Check photos
    var inputPhoto1 = document.getElementById("image1");
    var inputPhoto2 = document.getElementById("image2");
    var inputPhoto3 = document.getElementById("image3");

    if (! inputPhoto1.files.item(0).name.match(/\.(jpg|jpeg|png)$/)
        || ! inputPhoto2.files.item(0).name.match(/\.(jpg|jpeg|png)$/)
        || ! inputPhoto3.files.item(0).name.match(/\.(jpg|jpeg|png)$/)) {
        document.getElementById('error-form').style.display = 'inherit';
        document.getElementById("error-form")
            .innerHTML = "Erreur : Le format des images est invalide !" +
            " Uniquement les images au format PNG / JPG / JPEG est autoris&eacute;.";
        nbErrors++;
        return false;
    }
}

function enablePrice(value) {
    if (value == 2) {
        var div = document.getElementById('form-pvente');
        div.style.display = 'block';

        var div2 = document.getElementById('form-ploc');
        div2.style.display = 'none';
    }

    else if (value == 1) {
        var div = document.getElementById('form-ploc');
        div.style.display = 'block';

        var div2 = document.getElementById('form-pvente');
        div2.style.display = 'none';
    }
}