<?php

/**
 * Created by PhpStorm.
 * User: Romaric Mangin
 * Date: 10/21/2015
 * Time: 10:43 AM
 */

namespace configuration;

use Illuminate\Database\Capsule\Manager as Capsule;

class database
{
    public static function EloConfigure($filename)
    {
        $config = parse_ini_file($filename);

        if (!$config)
            throw new Exception("App::eloConfigure: could not parse config file $filename <br />");

        $capsule = new Capsule();
        $capsule->addConnection($config);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}

?>