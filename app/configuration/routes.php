<?php
/**
 * Created by PhpStorm.
 * User: Romaric Mangin
 * Date: 10/21/2015
 * Time: 11:01 AM
 */

use configuration\database as Database;
use app\models\Menu as Menu;
use app\models\TypeBien as TypeBien;
use app\models\TypeAnnonce as TypeAnnonce;

Database::EloConfigure('database.ini');


$req = $app::getInstance()->request();
$uri = $req->getUrl() . $req->getRootUri();


// Homepage
$app->get('/', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    require('../app/controllers/listAnnonceRecenteController.php');

    $recentAnnonce = $_SESSION['recentAnnonce_to_affiche'];
    unset($_SESSION['recentAnnonce_to_affiche']);

    // To get pictures
    if (isset($_SESSION['photos'])) {
        $photos = $_SESSION['photos'];
        unset($_SESSION['photos']);
    }
    else {
        $photos = null;
    }

    $app->render('homepage.twig', array('menuCollection' => $menu, "photos" => $photos,
    "recent" => $recentAnnonce, "url" => $uri));
});


// Add announce
$app->get('/ajout', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    $selectTypeBien = TypeBien::generateSelectAdd();
    $selectTypeAnnonce = TypeAnnonce::generateSelectAdd();

    $app->render('add.twig', array('menuCollection' => $menu, "url" => $uri,
        'selectTypeAnnonce' => $selectTypeAnnonce, 'selectTypeBien' => $selectTypeBien));
});

$app->post('/ajout', function() use ($app) {
    require('../app/controllers/AjoutController.php');
});


// Edit announce
$app->get('/edition/:id', function($id) use ($app, $uri) {
    $menu = Menu::generateCollection();

    $_SESSION['id_annonce_edit'] = $id;
    require('../app/controllers/EditionAnnonce.php');

    $pass = $_SESSION['annonce_password'];
    $app->render('edition.twig', array("menuCollection" => $menu, "url" => $uri, "pass" => $pass));
});

$app->post('/edition/:id', function($id) use ($app, $uri) {
    $menu = Menu::generateCollection();
    $selectTypeBien = TypeBien::generateSelectAdd();
    $selectTypeAnnonce = TypeAnnonce::generateSelectAdd();

    // First pass
    if (isset($_REQUEST['password'])) {
        $_SESSION['id_annonce_edit'] = $id;
        require('../app/controllers/EditionAnnoncePart2.php');

        if (isset($_SESSION['annonce_edition'])) {
            $announce = $_SESSION['annonce_edition'];

            $app->render('edition_form.twig', array('menuCollection' => $menu, "url" => $uri,
                'selectTypeAnnonce' => $selectTypeAnnonce, 'selectTypeBien' => $selectTypeBien, 'annonce' => $announce));

            unset($_SESSION['annonce_edition']);
        }
        unset($_SESSION['id_annonce_edit']);
    }
    // To update the announce
    else {
        $_SESSION['id_annonce_edit'] = $id;
        require('../app/controllers/EditionAnnoncePart3.php');
    }
});


// Delete announce
$app->get('/suppression/:id', function($id) use ($app, $uri) {
    $menu = Menu::generateCollection();

    $_SESSION['id_annonce_delete'] = $id;
    require('../app/controllers/SuppressionAnnonce.php');

    $pass = $_SESSION['annonce_password'];
    $app->render('suppression.twig', array("menuCollection" => $menu, "url" => $uri, "pass" => $pass));
});

$app->post('/suppression/:id', function($id) use ($app, $uri) {
    $_SESSION['id_annonce_delete'] = $id;
    require('../app/controllers/SuppressionAnnoncePart2.php');
});


// Display announce
$app->get('/annonce/:id', function($id) use ($app, $uri) {

    $menu = Menu::generateCollection();

    $_SESSION['id_annonce_a_afficher'] = $id;
    require('../app/controllers/AffichageAnnonceControleur.php');

    $annonce = $_SESSION['annonce_to_affiche'];
    unset($_SESSION['annonce_to_affiche']);
    $photos = $_SESSION['photos_to_affiche'];
    unset($_SESSION['photos_to_affiche']);
    $vendeur = $_SESSION['vendeur_to_affiche'];
    unset($_SESSION['vendeur_to_affiche']);
    $typeBien = $_SESSION['type_bien'];
    unset($_SESSION['type_bien']);

    // If in the prev page, bad pwd
    $req = $app->request;
    $ref = $req->getReferrer();

    if (strpos($ref,'suppression') !== false || strpos($ref,'edition') !== false) {
        // If announce edited now
        if (isset($_SESSION['edition_an'])) {
            $errorPass = 0;
            unset($_SESSION['edition_an']);
        }
        else {
            $errorPass = 1;
        }
    }
    else {
        $errorPass = 0;
    }

    $app->render('detailAnnonce.twig', array('menuCollection' => $menu, "errorPass" => $errorPass,
        "annonceG" => $annonce, "photosG" => $photos, "vendeurG" => $vendeur, "bien" => $typeBien, "url" => $uri));

});


// Search announce
$app->get('/recherche', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    $selectTypeBien = TypeBien::generateSelectAdd();
    $selectTypeAnnonce = TypeAnnonce::generateSelectAdd();

    $app->render('recherche_form.twig', array('menuCollection' => $menu, "url" => $uri,
        "selectTypeBien" => $selectTypeBien, "selectTypeAnnonce" => $selectTypeAnnonce));
});

$app->post('/recherche', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    require_once('../app/controllers/RechercheController.php');

    $annonce = $_SESSION['research_to_affiche'];
    unset($_SESSION['research_to_affiche']);

    // To get pictures
    if (isset($_SESSION['photos'])) {
        $photos = $_SESSION['photos'];
        unset($_SESSION['photos']);
    }
    else {
        $photos = null;
    }

    $app->render('recherche_resultat.twig', array('menuCollection' => $menu, "url" => $uri, "photos" => $photos,
        "research" => $annonce));
});


// Display person
$app->get('/vendeur/:id', function($id) use ($app, $uri) {
    $menu = Menu::generateCollection();

    unset($_SESSION['annonce']);
    $_SESSION['id_vendeur_affichage'] = $id;

    require('../app/controllers/AffichageVendeurController.php');

    if (isset($_SESSION['erreur_vendeur'])) {
        $dataVend = $_SESSION['erreur_vendeur'];
        unset($_SESSION['erreur_vendeur']);
    }
    else if (isset($_SESSION['vendeur'])) {
        $dataVend = $_SESSION['vendeur'];
        unset($_SESSION['vendeur']);
    }

    $app->render('vendeur.twig', array("menuCollection" => $menu, "url" => $uri, "donneesV" => $dataVend));
});


// If 404 not found
$app->notFound(function() use ($app, $uri) {
    $menu = Menu::generateCollection();

    $app->render('erreur.twig', array("menuCollection" => $menu, "url" => $uri));
});