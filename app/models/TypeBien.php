<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 25/10/2015
 * Time: 10:42
 */

namespace app\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeBien extends Eloquent {

    protected $table = 'typebien';
    protected $primaryKey = 'id_type';
    public $timestamps = false;

    public function annonceB(){
        return $this->hasMany('app\models\Annonce', 'idTypeBien');
    }

    public static function generateSelectAdd() {

        return (TypeBien::all());
    }
}