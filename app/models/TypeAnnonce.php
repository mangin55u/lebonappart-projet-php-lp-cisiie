<?php
namespace app\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeAnnonce extends Eloquent {
    protected $table = 'typeannonce';
    protected $primaryKey = 'id_type';
    public $timestamps = false;

    public function annonceT(){
        return $this->hasMany('app\models\Annonce', 'idTypeAnnonce');
    }

    public static function generateSelectAdd() {

        return (TypeAnnonce::all());
    }
}