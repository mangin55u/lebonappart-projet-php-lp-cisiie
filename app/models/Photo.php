<?php

namespace app\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Photo extends Eloquent{

    protected $table = 'photo';
    protected $primaryKey = 'id_photo';
    public $timestamps = false;

    public function annonce(){
        return $this->belongsTo('app\models\Annonce', 'id_annonce');
    }

}