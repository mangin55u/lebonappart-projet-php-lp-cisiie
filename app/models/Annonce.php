<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 25/10/2015
 * Time: 10:16
 */

namespace app\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Annonce extends Eloquent {
    protected $table = 'annonce';
    protected $primaryKey = 'id_annonce';
    public $timestamps = false;

    public function photos(){
        return $this->hasMany('app\models\Photo', "id_annonce");
    }

    public function typeAnnonce(){
        return $this->belongsTo('app\models\TypeAnnonce', "idTypeAnnonce");
    }

    public function vendeur(){
        return $this->belongsTo('app\models\Vendeur', 'idVendeur');
    }

    public function typebien(){
        return $this->belongsTo('app\models\TypeBien','idTypeBien');
    }
}