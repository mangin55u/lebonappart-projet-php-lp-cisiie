<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 25/10/2015
 * Time: 10:25
 */

namespace app\models;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Vendeur extends Eloquent {

    protected $table = 'vendeur';
    protected $primaryKey = 'id_vendeur';
    public $timestamps = false;

    public function annonceV(){
        return $this->hasMany('app\models\Annonce', 'idVendeur');
    }
}