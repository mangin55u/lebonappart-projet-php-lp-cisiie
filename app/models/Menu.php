<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 25/10/2015
 * Time: 10:35
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Menu extends Eloquent {
    protected $table = 'menu';
    protected $primaryKey = 'id_menu';
    public $timestamps = false;

    public static function generateCollection() {

        return (Menu::all());
    }
}