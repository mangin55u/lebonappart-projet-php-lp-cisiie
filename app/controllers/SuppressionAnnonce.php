<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 27/10/2015
 * Time: 08:07
 */

namespace app\controllers;

use app\models\Annonce as Annonce;


if (isset($_SESSION['id_annonce_delete'])) {
    $ann = Annonce::find($_SESSION['id_annonce_delete']);
    unset($_SESSION['id_annonce_delete']);

    if ($ann == null)
        $_SESSION['annonce_password'] = null;
    else
        $_SESSION['annonce_password'] = $ann->motDePasseAcces;
}