<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 27/10/2015
 * Time: 09:55
 */

namespace app\controllers;

use app\models\Annonce as Annonce;


if (strcmp(sha1(htmlspecialchars($_REQUEST['password'])), $_SESSION['annonce_password'])) {
    $idA = $_SESSION['id_annonce_edit'];
    unset($_SESSION['annonce_password']);
    unset($_SESSION['id_annonce_edit']);

    $app->redirect('../annonce/' . $idA);
}

// Good pass
else {
    $idA = $_SESSION['id_annonce_edit'];
    $ann = Annonce::find($idA);
    $_SESSION['annonce_edition'] = $ann;

    unset($_SESSION['annonce_password']);
    unset($_SESSION['id_annonce_edit']);
}