<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 27/10/2015
 * Time: 09:55
 */

namespace app\controllers;

use app\models\Annonce as Annonce;


if (strcmp(sha1(htmlspecialchars($_REQUEST['password'])), $_SESSION['annonce_password'])) {
    $idA = $_SESSION['id_annonce_delete'];
    unset($_SESSION['annonce_password']);
    unset($_SESSION['id_annonce_delete']);

    $app->redirect('../annonce/' . $idA);
}

// Good pass
else {
    $idA = $_SESSION['id_annonce_delete'];

    // Delete the annonce
    $ann = Annonce::find($idA);
    foreach ($ann->photos as $photo) {
        unlink($photo->url);
        $photo->delete();
    }
    $ann->delete();
    $ann->vendeur->delete();

    unset($_SESSION['annonce_password']);
    unset($_SESSION['id_annonce_delete']);

    $app->redirect('../');
}