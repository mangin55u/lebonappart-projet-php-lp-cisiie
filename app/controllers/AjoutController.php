<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 25/10/2015
 * Time: 09:35
 */
namespace app\controllers;
use app\models\Annonce as Annonce;
use app\models\Vendeur as Vendeur;
use app\models\Photo as Photo;

date_default_timezone_set('Europe/Paris');

if (!empty($_REQUEST) && !empty($_FILES)) {
    // Create the vendeur
    $vendeur = new Vendeur();
    $vendeur->nom = htmlspecialchars($_REQUEST['nom']);
    $vendeur->prenom = htmlspecialchars($_REQUEST['prenom']);
    $vendeur->email = htmlspecialchars($_REQUEST['email']);
    $vendeur->telephone = htmlspecialchars($_REQUEST['number']);
    $vendeur->save();


    // Create the annonce
    $annonce = new Annonce();
    $annonce->idTypeBien = htmlspecialchars($_REQUEST['bien']);
    $annonce->NomAnnonce = htmlspecialchars($_REQUEST['intitule']);
    $annonce->datePublication = htmlspecialchars(date("Y-m-d H:i:s"));
    $annonce->dateMiseAJour = htmlspecialchars(date("Y-m-d H:i:s"));
    $annonce->idVendeur = $vendeur->id_vendeur;
    $annonce->numeroRue = htmlspecialchars($_REQUEST['street_number']);
    $annonce->nomRue = htmlspecialchars($_REQUEST['route']);
    $annonce->ville = htmlspecialchars($_REQUEST['locality']);
    $annonce->codePostal = htmlspecialchars($_REQUEST['postal_code']);
    $annonce->idTypeAnnonce = htmlspecialchars($_REQUEST['tannonce']);
    $annonce->superficie = $_REQUEST['superficie'];

    if ($_REQUEST['tannonce'] == 1) {
        $annonce->prixVente = -1;
        $annonce->prixLoyerMensuel = $_REQUEST['ploc'];
    }
    else if ($_REQUEST['tannonce'] == 2) {
        $annonce->prixVente = $_REQUEST['pvente'];
        $annonce->prixLoyerMensuel = -1;
    }

    $annonce->descriptif = htmlspecialchars($_REQUEST['descriptif']);
    $annonce->nbPieces = $_REQUEST['pieces'];
    $annonce->garage = $_REQUEST['garage'];
    $annonce->piscine = $_REQUEST['piscine'];
    $annonce->jardin = $_REQUEST['jardin'];
    $annonce->motDePasseAcces = sha1($_REQUEST['password']);
    $annonce->save();


    $idA = $annonce->id_annonce;
    // Save the pictures
    for ($i = 1; $i < 4; $i++) {
        $tmpFile = $_FILES['image'.$i]['tmp_name'];
        $newFile = '../public/uploads/'.'annonce_'.$idA.'_image_'.$i.'_'.$_FILES['image'.$i]['name'];
        $newFile = preg_replace('/\s+/', '_', $newFile);

        $result = move_uploaded_file($tmpFile, $newFile);

        $photo = new Photo();
        $photo->url = $newFile;
        $photo->id_annonce = $idA;
        $photo->save();
    }


    $_SESSION['annonce'] = $idA;
    $app->redirect('./annonce/'.$_SESSION['annonce']);
}
?>