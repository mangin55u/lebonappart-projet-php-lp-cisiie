<?php

namespace app\controllers;
use app\models\Annonce;

$recentAnnonce = Annonce::orderBy('datePublication', 'DESC')->take(9)->get();
$_SESSION['recentAnnonce_to_affiche'] = $recentAnnonce;

if ($recentAnnonce != null) {
    $collectionPhotos = array();
    foreach ($recentAnnonce as $ann) {
        $photos = $ann->photos->take(1);

        foreach ($photos as $photo) {
            array_push($collectionPhotos, $photo->url);
        }
    }

    $_SESSION['photos'] = $collectionPhotos;
}