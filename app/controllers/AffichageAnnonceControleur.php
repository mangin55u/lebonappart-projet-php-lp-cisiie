<?php
namespace app\controllers;

use app\models\Annonce;

$id = $_SESSION['id_annonce_a_afficher'];
unset($_SESSION['id_annonce_a_afficher']);

$annonce = Annonce::find($id);
$_SESSION['annonce_to_affiche'] = $annonce;
if ($annonce == null) {
    $_SESSION['photos_to_affiche'] = null;
    $_SESSION['vendeur_to_affiche'] = null;
    $_SESSION['type_bien'] = null;
}
else {
    $_SESSION['photos_to_affiche'] = $annonce->photos;
    $_SESSION['vendeur_to_affiche'] = $annonce->vendeur;
    $_SESSION['type_bien'] = $annonce->typebien;
}

