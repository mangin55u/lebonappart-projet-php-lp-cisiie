<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 26/10/2015
 * Time: 13:52
 */

namespace app\controllers;
use app\models\Vendeur as Vendeur;

$id = $_SESSION['id_vendeur_affichage'];
unset($_SESSION['id_vendeur_affichage']);

$ven = Vendeur::find($id);
if ($ven == null) {
    $_SESSION['erreur_vendeur'] = 1;
}
else {
    $_SESSION['vendeur'] = array("prenom" => $ven->prenom, "email" => $ven->email, "telephone" => $ven->telephone);
}
?>