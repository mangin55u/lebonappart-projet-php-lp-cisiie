<?php

use app\models\Annonce;


$annonce = Annonce::where(function ($query) {
    $query->where("nomAnnonce", "like", '%'.htmlspecialchars($_REQUEST['intitule'].'%'))
        ->orwhere("descriptif", "like", "%".htmlspecialchars($_REQUEST['intitule']."%"));
})->where(function ($query) {
    $query->where("ville", "like", '%'.htmlspecialchars($_REQUEST['ville'].'%'))
        ->where("idTypeBien","=", htmlspecialchars($_REQUEST['bien']))
        ->where("idTypeAnnonce","=",htmlspecialchars($_REQUEST['tannonce']))
        ->where("prixVente", "<=", htmlspecialchars($_REQUEST['pmax']))
        ->where("nbPieces", ">=", htmlspecialchars($_REQUEST['piecemin']));
})->get();

$_SESSION['research_to_affiche'] = $annonce;


if ($annonce != null) {
    $collectionPhotos = array();
    foreach ($annonce as $ann) {
        $photos = $ann->photos->take(1);

        foreach ($photos as $photo) {
            array_push($collectionPhotos, $photo->url);
        }
    }

    $_SESSION['photos'] = $collectionPhotos;
}