<?php
/**
 * Created by PhpStorm.
 * User: Romaric
 * Date: 27/10/2015
 * Time: 09:55
 */

namespace app\controllers;

use app\models\Annonce as Annonce;


if (isset($_REQUEST) && isset($_SESSION['id_annonce_edit'])) {
    $id = $_SESSION['id_annonce_edit'];
    unset($_SESSION['id_annonce_edit']);

    $ann = Annonce::find($id);
    $ann->idTypeBien = htmlspecialchars($_REQUEST['bien']);
    $ann->NomAnnonce = htmlspecialchars($_REQUEST['intitule']);
    $ann->dateMiseAJour = htmlspecialchars(date("Y-m-d H:i:s"));
    $ann->numeroRue = htmlspecialchars($_REQUEST['street_number']);
    $ann->nomRue = htmlspecialchars($_REQUEST['route']);
    $ann->ville = htmlspecialchars($_REQUEST['locality']);
    $ann->codePostal = htmlspecialchars($_REQUEST['postal_code']);
    $ann->idTypeAnnonce = htmlspecialchars($_REQUEST['tannonce']);
    $ann->superficie = $_REQUEST['superficie'];

    if ($_REQUEST['tannonce'] == 1) {
        $ann->prixVente = -1;
        $ann->prixLoyerMensuel = $_REQUEST['ploc'];
    }
    else if ($_REQUEST['tannonce'] == 2) {
        $ann->prixVente = $_REQUEST['pvente'];
        $ann->prixLoyerMensuel = -1;
    }

    $ann->descriptif = htmlspecialchars($_REQUEST['descriptif']);
    $ann->nbPieces = $_REQUEST['pieces'];
    $ann->garage = $_REQUEST['garage'];
    $ann->piscine = $_REQUEST['piscine'];
    $ann->jardin = $_REQUEST['jardin'];


    $_SESSION['edition_an'] = 1;

    $ann->save();
    $app->redirect('../annonce/'.$id);
}