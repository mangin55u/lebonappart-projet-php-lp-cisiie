# Projet Web : LeBonAppart #


*LeBonAppart* est un site Internet réalisé dans le cadre du module de Programmation Web orientée Objet. Les technologies utilisées ont été celles étudiées en cours :
###  ###
* TwigComposer
* Eloquent ORM
* Micro-framework PHP Slim
* PHP orienté objet
###  ###
En complément de ces technologies, il a été utilisé HTML ainsi que JavaScript et la librairie JQuery pour rendre le site plus attractif ainsi que les feuilles de styles CSS générées à l’aide du pré-processeur Sass.


## **Membres du groupe** ##
* Anthony BARBIER
* Romaric MANGIN
* Corentin VUILLAUME


## **Source Bitbucket** ##
https://bitbucket.org/mangin55u/lebonappart-projet-php-lp-cisiie


## **Utilisation** ##
Afin d’utiliser le code source, fourni sur BitBucket, vous devez tout d’abord disposer d’un serveur Web, Apache est conseillé. Il est également nécessaire de disposer d’une base de données au format MySQL.


## **Pré-requis** ##
* Le module Rewrite de Apache activé
* Sur les systèmes Unix, il est nécessaire d’ajouter ces lignes au virtualhost de votre serveur Web Apache


```
#!apache

<Directory "/var/www/html">
    AllowOverride All
</Directory>
```

* L’importation de la base de données nécessaire au site Internet. Le fichier .sql se trouve dans le dossier app/script/ du code source fourni
* Un fichier .ini nécessaire à Eloquent pour se connecter à cette base de données

```
#!ini

[database]
driver = "mysql"
host = "localhost"
database = "lebonappart"
username = "root"
password = ""
charset = "utf8"
collation = "utf8_unicode_ci"
prefix = ""
```


* D’éxécuter la commande “composer update” afin de télécharger les packages nécessaires au site Internet


## **Contribution personnelle de chacun** ##

*Romaric* 
###  ###
* Partie conception réalisée avec Anthony
* Mise en place du projet sur Bitbucket
* Ajout d’une annonce ainsi que sa route associée
* Modification et suppression d’une annonce par l’intermédiaire d’un mot de crypté par le protocole SHA-1 dans la base de données
* Ajout de l’interface permettant d’avoir les coordonnées d’un vendeur
* Gestion de l’interface front-end / clients grâce à Twig et aux feuilles de styles
* Validation du code HTML avec le validateur du W3C
* Également aidé Anthony à la création / liaison des différents modèles des objets de la base de données sous la forme d’objets grâce à l’ORM Eloquent

###  ###
###  ###

*Anthony* 
###  ###
* Avec Romaric, mise en place de la conception du projet : diagramme de classe, diagrammes de séquence, diagramme de cas d’utilisation et schéma pour la base de données
* Afficher le détail d’une annonce + route associée 
* Afficher une liste de 9 annonces les plus récentes sur la page d’accueil, avec un lien renvoyant sur le détail de l’annonce + route associée
* Rechercher une annonce : par mot clé, lieu, budget max, location/vente, maison/appartement, nombre de pièces minimum + route associée
* Affichage de la liste des annonces en fonction du résultat de la recherche
* Ajout d’annonces fictives à la base de données pour illustrer le projet achevé

###  ###
###  ###

*Corentin*
###  ###
* Élaboration des diagrammes de séquence et de cas d’utilisation sur Lucidchart
* Rechercher une annonce
* Aide à la mise en place de Twig